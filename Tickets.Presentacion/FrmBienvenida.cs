﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tickets.Presentacion
{
    public partial class FrmBienvenida : Form
    {
        public FrmBienvenida()
        {
            InitializeComponent();

            //Cursor.Hide();
            LblPlataforma.Text = "Servicios brindados en PLATAFORMA:\n"
                + "\n- Instalaciones nuevas y cambios de nombre"
                + "\n- Traslados,cambios y acometida de medidores"
                + "\n- Suspensiones y rehabilitaciones"
                + "\n- Pago de dividendos"
                + "\n- Otros referidos a Atención al Cliente";
            LblOdeco.Text = "Servicios brindados en ODECO:\n"
                + "\n- Reclamos"
                + "\n- Descuentos o privilegios de la 3ra Edad"
                + "\n- Descuentos o privilegios a Personas con Capacidades Distintas";

            LblCreditos.Text = "Departamento de Tecnologías de Información y Comunicación - " + DateTime.Today.Year + " © Compañía Eléctrica Sucre S. A.";
        }

        private void FrmInicio_Load(object sender, EventArgs e)
        {
            
        }

        private void BtnPlataforma_Click(object sender, EventArgs e)
        {
            var frmMenuPlataforma = new FrmMenuPlataforma(ref LblUltimoTicket);
            frmMenuPlataforma.ShowDialog();
        }

        private void BtnOdeco_Click(object sender, EventArgs e)
        {
            var frmMenuOdeco = new FrmMenuOdeco(ref LblUltimoTicket);
            frmMenuOdeco.ShowDialog();
        }

    }
}
