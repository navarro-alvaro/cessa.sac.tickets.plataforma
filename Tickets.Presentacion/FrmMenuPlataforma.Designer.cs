﻿namespace Tickets.Presentacion
{
    partial class FrmMenuPlataforma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TlpPrincipal = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.LblPlataforma = new System.Windows.Forms.Label();
            this.LblOdeco = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.TlpIzquierda = new System.Windows.Forms.TableLayoutPanel();
            this.LblBienvenido = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.BtnPlataforma = new System.Windows.Forms.Button();
            this.BtnPlataformaEmbarazadas = new System.Windows.Forms.Button();
            this.BtnPlataforma3raEdad = new System.Windows.Forms.Button();
            this.LblCreditos = new System.Windows.Forms.Label();
            this.TmrPlataforma = new System.Windows.Forms.Timer(this.components);
            this.PbxLogo = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.BtnPlataformaCapacidadesDistintas = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.TlpPrincipal.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.TlpIzquierda.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbxLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // TlpPrincipal
            // 
            this.TlpPrincipal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TlpPrincipal.BackColor = System.Drawing.Color.White;
            this.TlpPrincipal.ColumnCount = 1;
            this.TlpPrincipal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TlpPrincipal.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.TlpPrincipal.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.TlpPrincipal.Controls.Add(this.LblCreditos, 0, 2);
            this.TlpPrincipal.Location = new System.Drawing.Point(17, 17);
            this.TlpPrincipal.Margin = new System.Windows.Forms.Padding(10);
            this.TlpPrincipal.Name = "TlpPrincipal";
            this.TlpPrincipal.Padding = new System.Windows.Forms.Padding(5);
            this.TlpPrincipal.RowCount = 3;
            this.TlpPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.TlpPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.TlpPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.TlpPrincipal.Size = new System.Drawing.Size(808, 568);
            this.TlpPrincipal.TabIndex = 3;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.LblPlataforma, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.LblOdeco, 0, 0);
            this.tableLayoutPanel3.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel3.Location = new System.Drawing.Point(10, 359);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.Padding = new System.Windows.Forms.Padding(20);
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 138F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(788, 178);
            this.tableLayoutPanel3.TabIndex = 5;
            // 
            // LblPlataforma
            // 
            this.LblPlataforma.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LblPlataforma.AutoSize = true;
            this.LblPlataforma.BackColor = System.Drawing.Color.Black;
            this.LblPlataforma.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPlataforma.ForeColor = System.Drawing.Color.White;
            this.LblPlataforma.Location = new System.Drawing.Point(23, 20);
            this.LblPlataforma.Name = "LblPlataforma";
            this.LblPlataforma.Size = new System.Drawing.Size(368, 138);
            this.LblPlataforma.TabIndex = 4;
            this.LblPlataforma.Text = "Servicios brindados en PLATAFORMA:";
            // 
            // LblOdeco
            // 
            this.LblOdeco.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LblOdeco.AutoSize = true;
            this.LblOdeco.BackColor = System.Drawing.Color.Black;
            this.LblOdeco.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblOdeco.ForeColor = System.Drawing.Color.White;
            this.LblOdeco.Location = new System.Drawing.Point(397, 20);
            this.LblOdeco.Name = "LblOdeco";
            this.LblOdeco.Size = new System.Drawing.Size(368, 138);
            this.LblOdeco.TabIndex = 3;
            this.LblOdeco.Text = "Servicios brindados en ODECO:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.TlpIzquierda, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(8, 8);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 343F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(792, 343);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // TlpIzquierda
            // 
            this.TlpIzquierda.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TlpIzquierda.BackColor = System.Drawing.Color.White;
            this.TlpIzquierda.ColumnCount = 1;
            this.TlpIzquierda.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TlpIzquierda.Controls.Add(this.LblBienvenido, 0, 0);
            this.TlpIzquierda.Controls.Add(this.PbxLogo, 0, 1);
            this.TlpIzquierda.Location = new System.Drawing.Point(3, 3);
            this.TlpIzquierda.Name = "TlpIzquierda";
            this.TlpIzquierda.Padding = new System.Windows.Forms.Padding(10);
            this.TlpIzquierda.RowCount = 2;
            this.TlpIzquierda.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.TlpIzquierda.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TlpIzquierda.Size = new System.Drawing.Size(390, 337);
            this.TlpIzquierda.TabIndex = 2;
            // 
            // LblBienvenido
            // 
            this.LblBienvenido.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LblBienvenido.AutoSize = true;
            this.LblBienvenido.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBienvenido.Location = new System.Drawing.Point(13, 10);
            this.LblBienvenido.Name = "LblBienvenido";
            this.LblBienvenido.Size = new System.Drawing.Size(364, 80);
            this.LblBienvenido.TabIndex = 2;
            this.LblBienvenido.Text = "Compañía Eléctrica Sucre S. A.";
            this.LblBienvenido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.DarkBlue;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.BtnPlataforma, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.BtnPlataformaEmbarazadas, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.BtnPlataforma3raEdad, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox3, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.BtnPlataformaCapacidadesDistintas, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox4, 0, 4);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(399, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.Padding = new System.Windows.Forms.Padding(20);
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(390, 337);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // BtnPlataforma
            // 
            this.BtnPlataforma.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnPlataforma.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPlataforma.Location = new System.Drawing.Point(180, 50);
            this.BtnPlataforma.Margin = new System.Windows.Forms.Padding(10);
            this.BtnPlataforma.Name = "BtnPlataforma";
            this.BtnPlataforma.Size = new System.Drawing.Size(180, 44);
            this.BtnPlataforma.TabIndex = 6;
            this.BtnPlataforma.TabStop = false;
            this.BtnPlataforma.Text = "PLATAFORMA";
            this.BtnPlataforma.UseVisualStyleBackColor = true;
            this.BtnPlataforma.Click += new System.EventHandler(this.BtnPlataforma_Click);
            // 
            // BtnPlataformaEmbarazadas
            // 
            this.BtnPlataformaEmbarazadas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnPlataformaEmbarazadas.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPlataformaEmbarazadas.Location = new System.Drawing.Point(180, 178);
            this.BtnPlataformaEmbarazadas.Margin = new System.Windows.Forms.Padding(10);
            this.BtnPlataformaEmbarazadas.Name = "BtnPlataformaEmbarazadas";
            this.BtnPlataformaEmbarazadas.Size = new System.Drawing.Size(180, 44);
            this.BtnPlataformaEmbarazadas.TabIndex = 8;
            this.BtnPlataformaEmbarazadas.TabStop = false;
            this.BtnPlataformaEmbarazadas.Text = "PLATAFORMA EMBARAZADAS";
            this.BtnPlataformaEmbarazadas.UseVisualStyleBackColor = true;
            this.BtnPlataformaEmbarazadas.Click += new System.EventHandler(this.BtnPlataformaEmbarazadas_Click);
            // 
            // BtnPlataforma3raEdad
            // 
            this.BtnPlataforma3raEdad.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnPlataforma3raEdad.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPlataforma3raEdad.Location = new System.Drawing.Point(180, 114);
            this.BtnPlataforma3raEdad.Margin = new System.Windows.Forms.Padding(10);
            this.BtnPlataforma3raEdad.Name = "BtnPlataforma3raEdad";
            this.BtnPlataforma3raEdad.Size = new System.Drawing.Size(180, 44);
            this.BtnPlataforma3raEdad.TabIndex = 10;
            this.BtnPlataforma3raEdad.TabStop = false;
            this.BtnPlataforma3raEdad.Text = "PLATAFORMA 3RA EDAD";
            this.BtnPlataforma3raEdad.UseVisualStyleBackColor = true;
            this.BtnPlataforma3raEdad.Click += new System.EventHandler(this.BtnPlataforma3raEdad_Click);
            // 
            // LblCreditos
            // 
            this.LblCreditos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LblCreditos.AutoSize = true;
            this.LblCreditos.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCreditos.Location = new System.Drawing.Point(8, 542);
            this.LblCreditos.Name = "LblCreditos";
            this.LblCreditos.Size = new System.Drawing.Size(792, 21);
            this.LblCreditos.TabIndex = 4;
            this.LblCreditos.Text = "Unidad de Tecnologías de Información y Comunicación - 2015 © Compañía Eléctrica S" +
    "ucre S. A.";
            this.LblCreditos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TmrPlataforma
            // 
            this.TmrPlataforma.Interval = 1000;
            this.TmrPlataforma.Tick += new System.EventHandler(this.TmrPlataforma_Tick);
            // 
            // PbxLogo
            // 
            this.PbxLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PbxLogo.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.PbxLogo.ErrorImage = null;
            this.PbxLogo.Image = global::Tickets.Presentacion.Properties.Resources.logo_cessa;
            this.PbxLogo.InitialImage = null;
            this.PbxLogo.Location = new System.Drawing.Point(13, 93);
            this.PbxLogo.Name = "PbxLogo";
            this.PbxLogo.Size = new System.Drawing.Size(364, 231);
            this.PbxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PbxLogo.TabIndex = 0;
            this.PbxLogo.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::Tickets.Presentacion.Properties.Resources.persona_normal;
            this.pictureBox1.Location = new System.Drawing.Point(23, 43);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(144, 58);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.Image = global::Tickets.Presentacion.Properties.Resources.persona_3ra_edad;
            this.pictureBox2.Location = new System.Drawing.Point(23, 107);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(144, 58);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 12;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Image = global::Tickets.Presentacion.Properties.Resources.persona_embarazada;
            this.pictureBox3.Location = new System.Drawing.Point(23, 171);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(144, 58);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 13;
            this.pictureBox3.TabStop = false;
            // 
            // BtnPlataformaCapacidadesDistintas
            // 
            this.BtnPlataformaCapacidadesDistintas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnPlataformaCapacidadesDistintas.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPlataformaCapacidadesDistintas.Location = new System.Drawing.Point(180, 242);
            this.BtnPlataformaCapacidadesDistintas.Margin = new System.Windows.Forms.Padding(10);
            this.BtnPlataformaCapacidadesDistintas.Name = "BtnPlataformaCapacidadesDistintas";
            this.BtnPlataformaCapacidadesDistintas.Size = new System.Drawing.Size(180, 44);
            this.BtnPlataformaCapacidadesDistintas.TabIndex = 14;
            this.BtnPlataformaCapacidadesDistintas.TabStop = false;
            this.BtnPlataformaCapacidadesDistintas.Text = "PLATAFORMA EMBARAZADAS";
            this.BtnPlataformaCapacidadesDistintas.UseVisualStyleBackColor = true;
            this.BtnPlataformaCapacidadesDistintas.Click += new System.EventHandler(this.BtnPlataformaCapacidadesDistintas_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.Image = global::Tickets.Presentacion.Properties.Resources.persona_capacidades_distintas;
            this.pictureBox4.Location = new System.Drawing.Point(23, 235);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(144, 58);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 15;
            this.pictureBox4.TabStop = false;
            // 
            // FrmMenuPlataforma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSlateGray;
            this.ClientSize = new System.Drawing.Size(840, 600);
            this.Controls.Add(this.TlpPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmMenuPlataforma";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Text = "FrmMenu";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.TlpPrincipal.ResumeLayout(false);
            this.TlpPrincipal.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.TlpIzquierda.ResumeLayout(false);
            this.TlpIzquierda.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PbxLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TlpPrincipal;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label LblPlataforma;
        private System.Windows.Forms.Label LblOdeco;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel TlpIzquierda;
        private System.Windows.Forms.Label LblBienvenido;
        private System.Windows.Forms.PictureBox PbxLogo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button BtnPlataforma;
        private System.Windows.Forms.Button BtnPlataformaEmbarazadas;
        private System.Windows.Forms.Label LblCreditos;
        private System.Windows.Forms.Button BtnPlataforma3raEdad;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Timer TmrPlataforma;
        private System.Windows.Forms.Button BtnPlataformaCapacidadesDistintas;
        private System.Windows.Forms.PictureBox pictureBox4;

    }
}