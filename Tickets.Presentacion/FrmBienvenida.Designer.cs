﻿namespace Tickets.Presentacion
{
    partial class FrmBienvenida
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TlpPrincipal = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.LblPlataforma = new System.Windows.Forms.Label();
            this.LblOdeco = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.TlpIzquierda = new System.Windows.Forms.TableLayoutPanel();
            this.LblBienvenido = new System.Windows.Forms.Label();
            this.PbxLogo = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.LblUltimoTicket = new System.Windows.Forms.Label();
            this.PbxPlataforma = new System.Windows.Forms.PictureBox();
            this.PbxOdeco = new System.Windows.Forms.PictureBox();
            this.BtnPlataforma = new System.Windows.Forms.Button();
            this.BtnOdeco = new System.Windows.Forms.Button();
            this.LblPreguntaOpciones = new System.Windows.Forms.Label();
            this.LblCreditos = new System.Windows.Forms.Label();
            this.TlpPrincipal.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.TlpIzquierda.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbxLogo)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbxPlataforma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbxOdeco)).BeginInit();
            this.SuspendLayout();
            // 
            // TlpPrincipal
            // 
            this.TlpPrincipal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TlpPrincipal.BackColor = System.Drawing.Color.White;
            this.TlpPrincipal.ColumnCount = 1;
            this.TlpPrincipal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TlpPrincipal.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.TlpPrincipal.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.TlpPrincipal.Controls.Add(this.LblCreditos, 0, 2);
            this.TlpPrincipal.Location = new System.Drawing.Point(17, 17);
            this.TlpPrincipal.Margin = new System.Windows.Forms.Padding(10);
            this.TlpPrincipal.Name = "TlpPrincipal";
            this.TlpPrincipal.Padding = new System.Windows.Forms.Padding(5);
            this.TlpPrincipal.RowCount = 3;
            this.TlpPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.TlpPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.TlpPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.TlpPrincipal.Size = new System.Drawing.Size(806, 566);
            this.TlpPrincipal.TabIndex = 2;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.LblPlataforma, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.LblOdeco, 0, 0);
            this.tableLayoutPanel3.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel3.Location = new System.Drawing.Point(10, 358);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.Padding = new System.Windows.Forms.Padding(20);
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 127F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(786, 177);
            this.tableLayoutPanel3.TabIndex = 5;
            // 
            // LblPlataforma
            // 
            this.LblPlataforma.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LblPlataforma.AutoSize = true;
            this.LblPlataforma.BackColor = System.Drawing.Color.Black;
            this.LblPlataforma.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPlataforma.ForeColor = System.Drawing.Color.White;
            this.LblPlataforma.Location = new System.Drawing.Point(23, 20);
            this.LblPlataforma.Name = "LblPlataforma";
            this.LblPlataforma.Size = new System.Drawing.Size(367, 137);
            this.LblPlataforma.TabIndex = 4;
            this.LblPlataforma.Text = "Servicios brindados en PLATAFORMA:";
            // 
            // LblOdeco
            // 
            this.LblOdeco.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LblOdeco.AutoSize = true;
            this.LblOdeco.BackColor = System.Drawing.Color.Black;
            this.LblOdeco.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblOdeco.ForeColor = System.Drawing.Color.White;
            this.LblOdeco.Location = new System.Drawing.Point(396, 20);
            this.LblOdeco.Name = "LblOdeco";
            this.LblOdeco.Size = new System.Drawing.Size(367, 137);
            this.LblOdeco.TabIndex = 3;
            this.LblOdeco.Text = "Servicios brindados en ODECO:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.TlpIzquierda, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(8, 8);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 322F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(790, 342);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // TlpIzquierda
            // 
            this.TlpIzquierda.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TlpIzquierda.BackColor = System.Drawing.Color.White;
            this.TlpIzquierda.ColumnCount = 1;
            this.TlpIzquierda.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TlpIzquierda.Controls.Add(this.LblBienvenido, 0, 0);
            this.TlpIzquierda.Controls.Add(this.PbxLogo, 0, 1);
            this.TlpIzquierda.Location = new System.Drawing.Point(3, 3);
            this.TlpIzquierda.Name = "TlpIzquierda";
            this.TlpIzquierda.Padding = new System.Windows.Forms.Padding(10);
            this.TlpIzquierda.RowCount = 2;
            this.TlpIzquierda.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.TlpIzquierda.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TlpIzquierda.Size = new System.Drawing.Size(389, 336);
            this.TlpIzquierda.TabIndex = 2;
            // 
            // LblBienvenido
            // 
            this.LblBienvenido.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LblBienvenido.AutoSize = true;
            this.LblBienvenido.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBienvenido.Location = new System.Drawing.Point(13, 10);
            this.LblBienvenido.Name = "LblBienvenido";
            this.LblBienvenido.Size = new System.Drawing.Size(363, 80);
            this.LblBienvenido.TabIndex = 2;
            this.LblBienvenido.Text = "Compañía Eléctrica Sucre S. A.";
            this.LblBienvenido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PbxLogo
            // 
            this.PbxLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PbxLogo.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.PbxLogo.ErrorImage = null;
            this.PbxLogo.Image = global::Tickets.Presentacion.Properties.Resources.logo_cessa;
            this.PbxLogo.InitialImage = null;
            this.PbxLogo.Location = new System.Drawing.Point(13, 93);
            this.PbxLogo.Name = "PbxLogo";
            this.PbxLogo.Size = new System.Drawing.Size(363, 230);
            this.PbxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PbxLogo.TabIndex = 0;
            this.PbxLogo.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.DarkBlue;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.LblUltimoTicket, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.PbxPlataforma, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.PbxOdeco, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.BtnPlataforma, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.BtnOdeco, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.LblPreguntaOpciones, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(398, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.Padding = new System.Windows.Forms.Padding(20);
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(389, 336);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // LblUltimoTicket
            // 
            this.LblUltimoTicket.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LblUltimoTicket.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.LblUltimoTicket, 2);
            this.LblUltimoTicket.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblUltimoTicket.ForeColor = System.Drawing.Color.Yellow;
            this.LblUltimoTicket.Location = new System.Drawing.Point(23, 276);
            this.LblUltimoTicket.Name = "LblUltimoTicket";
            this.LblUltimoTicket.Size = new System.Drawing.Size(343, 40);
            this.LblUltimoTicket.TabIndex = 10;
            this.LblUltimoTicket.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PbxPlataforma
            // 
            this.PbxPlataforma.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PbxPlataforma.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PbxPlataforma.Image = global::Tickets.Presentacion.Properties.Resources.btn_plataforma_90x90;
            this.PbxPlataforma.Location = new System.Drawing.Point(23, 103);
            this.PbxPlataforma.Name = "PbxPlataforma";
            this.PbxPlataforma.Size = new System.Drawing.Size(144, 82);
            this.PbxPlataforma.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PbxPlataforma.TabIndex = 4;
            this.PbxPlataforma.TabStop = false;
            // 
            // PbxOdeco
            // 
            this.PbxOdeco.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PbxOdeco.Image = global::Tickets.Presentacion.Properties.Resources.btn_odeco_90x90;
            this.PbxOdeco.Location = new System.Drawing.Point(23, 191);
            this.PbxOdeco.Name = "PbxOdeco";
            this.PbxOdeco.Size = new System.Drawing.Size(144, 82);
            this.PbxOdeco.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PbxOdeco.TabIndex = 5;
            this.PbxOdeco.TabStop = false;
            // 
            // BtnPlataforma
            // 
            this.BtnPlataforma.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnPlataforma.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPlataforma.Location = new System.Drawing.Point(180, 110);
            this.BtnPlataforma.Margin = new System.Windows.Forms.Padding(10);
            this.BtnPlataforma.Name = "BtnPlataforma";
            this.BtnPlataforma.Size = new System.Drawing.Size(179, 68);
            this.BtnPlataforma.TabIndex = 6;
            this.BtnPlataforma.TabStop = false;
            this.BtnPlataforma.Text = "PLATAFORMA";
            this.BtnPlataforma.UseVisualStyleBackColor = true;
            this.BtnPlataforma.Click += new System.EventHandler(this.BtnPlataforma_Click);
            // 
            // BtnOdeco
            // 
            this.BtnOdeco.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnOdeco.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnOdeco.Location = new System.Drawing.Point(180, 198);
            this.BtnOdeco.Margin = new System.Windows.Forms.Padding(10);
            this.BtnOdeco.Name = "BtnOdeco";
            this.BtnOdeco.Size = new System.Drawing.Size(179, 68);
            this.BtnOdeco.TabIndex = 8;
            this.BtnOdeco.TabStop = false;
            this.BtnOdeco.Text = "ODECO";
            this.BtnOdeco.UseVisualStyleBackColor = true;
            this.BtnOdeco.Click += new System.EventHandler(this.BtnOdeco_Click);
            // 
            // LblPreguntaOpciones
            // 
            this.LblPreguntaOpciones.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LblPreguntaOpciones.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.LblPreguntaOpciones, 2);
            this.LblPreguntaOpciones.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPreguntaOpciones.ForeColor = System.Drawing.Color.Yellow;
            this.LblPreguntaOpciones.Location = new System.Drawing.Point(23, 20);
            this.LblPreguntaOpciones.Name = "LblPreguntaOpciones";
            this.LblPreguntaOpciones.Size = new System.Drawing.Size(343, 80);
            this.LblPreguntaOpciones.TabIndex = 9;
            this.LblPreguntaOpciones.Text = "¿Qué sección desea consultar?";
            this.LblPreguntaOpciones.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblCreditos
            // 
            this.LblCreditos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LblCreditos.AutoSize = true;
            this.LblCreditos.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCreditos.Location = new System.Drawing.Point(8, 540);
            this.LblCreditos.Name = "LblCreditos";
            this.LblCreditos.Size = new System.Drawing.Size(790, 21);
            this.LblCreditos.TabIndex = 4;
            this.LblCreditos.Text = "Unidad de Tecnologías de Información y Comunicación - 2015 © Compañía Eléctrica S" +
    "ucre S. A.";
            this.LblCreditos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmBienvenida
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(840, 600);
            this.Controls.Add(this.TlpPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmBienvenida";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Text = "FrmInicio";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmInicio_Load);
            this.TlpPrincipal.ResumeLayout(false);
            this.TlpPrincipal.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.TlpIzquierda.ResumeLayout(false);
            this.TlpIzquierda.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbxLogo)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbxPlataforma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbxOdeco)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TlpPrincipal;
        private System.Windows.Forms.Label LblCreditos;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel TlpIzquierda;
        private System.Windows.Forms.Label LblBienvenido;
        private System.Windows.Forms.PictureBox PbxLogo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.PictureBox PbxPlataforma;
        private System.Windows.Forms.PictureBox PbxOdeco;
        private System.Windows.Forms.Button BtnPlataforma;
        private System.Windows.Forms.Button BtnOdeco;
        private System.Windows.Forms.Label LblPreguntaOpciones;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label LblOdeco;
        private System.Windows.Forms.Label LblPlataforma;
        private System.Windows.Forms.Label LblUltimoTicket;
    }
}