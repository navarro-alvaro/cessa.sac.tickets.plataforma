﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tickets.Presentacion
{
    public partial class FrmMenuPlataforma : Form
    {
        Negocio.Ticket ticketNegocio;
        RptTicket Reporte;
        private int tiempoPantalla;
        private Label ultimoTicket;

        public FrmMenuPlataforma(ref Label ultimoTicketForm)
        {
            InitializeComponent();

            //Cursor.Hide();
            BtnPlataforma3raEdad.Text = "PLATAFORMA\n3RA EDAD";
            BtnPlataformaEmbarazadas.Text = "PLATAFORMA\nEMBARAZADAS";
            BtnPlataformaCapacidadesDistintas.Text = "PLATAFORMA\nCAPACIDADES DISTINTAS";
            LblPlataforma.Text = "Servicios brindados en PLATAFORMA:\n"
                + "\n- Instalaciones nuevas y cambios de nombre"
                + "\n- Traslados,cambios y acometida de medidores"
                + "\n- Suspensiones y rehabilitaciones"
                + "\n- Pago de dividendos"
                + "\n- Otros referidos a Atención al Cliente";
            LblOdeco.Text = "Servicios brindados en ODECO:\n"
                + "\n- Reclamos"
                + "\n- Descuentos o privilegios de la 3ra Edad"
                + "\n- Descuentos o privilegios a Personas con Capacidades Distintas";

            ultimoTicket = ultimoTicketForm;
            ticketNegocio = new Negocio.Ticket();
            ticketNegocio.Errores += new Negocio.Ticket.ErroresEventHandler(ticket_Errores);
            ticketNegocio.MostrarInicio += new Negocio.Ticket.MostrarInicioEventHandler(ticket_MostrarInicio);

            TmrPlataforma.Enabled = true;
            tiempoPantalla = 0;

            LblCreditos.Text = "Departamento de Tecnologías de Información y Comunicación - " + DateTime.Today.Year + " © Compañía Eléctrica Sucre S. A.";

            Reporte = new RptTicket();
        }

        void ticket_MostrarInicio()
        {
            ultimoTicket.Text = "El último Ticket generado fue el " + ticketNegocio.Tipo + "-" + ticketNegocio.Numero.ToString().PadLeft(3, '0');

            Reporte.Imprimir(
                ticketNegocio.Tipo + "-" + ticketNegocio.Numero.ToString().PadLeft(3, '0'),
                ticketNegocio.FechaSolicitud + " " + ticketNegocio.HoraSolicitud
            );

            this.Close();
        }

        void ticket_Errores(string mensaje)
        {
            MessageBox.Show(mensaje, "Ocurrió un error");
        }

        private void TmrPlataforma_Tick(object sender, EventArgs e)
        {
            tiempoPantalla++;
            if (tiempoPantalla == 10)
            {
                TmrPlataforma.Enabled = false;
                this.Close();
            }
        }

        private void BtnPlataforma_Click(object sender, EventArgs e)
        {
            ticketNegocio.Tipo = "A";
            ticketNegocio.Generar();
        }

        private void BtnPlataforma3raEdad_Click(object sender, EventArgs e)
        {
            ticketNegocio.Tipo = "B";
            ticketNegocio.Generar();
        }

        private void BtnPlataformaEmbarazadas_Click(object sender, EventArgs e)
        {
            ticketNegocio.Tipo = "C";
            ticketNegocio.Generar();
        }

        private void BtnPlataformaCapacidadesDistintas_Click(object sender, EventArgs e)
        {
            ticketNegocio.Tipo = "B";
            ticketNegocio.Generar();
        }

        
    }
}
