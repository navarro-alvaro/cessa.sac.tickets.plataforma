﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace Tickets.Datos
{
    public class Ticket
    {
        public Ticket()
        {
            ticketEnUso.Tipo = string.Empty;
            ticketEnUso.Numero = 0;
        }

        //Estructura para almacenar la información
        private struct TTicket
        {
            public string Tipo;
            public int Numero;
            public string FechaSolicitud;
            public string HoraSolicitud;
            public string HoraAtencion;
        }
        TTicket ticketEnUso = new TTicket();

        //Propiedades
        public string Tipo
        {
            get { return ticketEnUso.Tipo; }
            set { ticketEnUso.Tipo = value; }
        }
        public int Numero
        {
            get { return ticketEnUso.Numero; }
            set { ticketEnUso.Numero = value; }
        }
        public string FechaSolicitud
        {
            get { return ticketEnUso.FechaSolicitud; }
            set { ticketEnUso.FechaSolicitud = value; }
        }
        public string HoraSolicitud
        {
            get { return ticketEnUso.HoraSolicitud; }
            set { ticketEnUso.HoraSolicitud = value; }
        }
        public string HoraAtencion
        {
            get { return ticketEnUso.HoraAtencion; }
            set { ticketEnUso.HoraAtencion = value; }
        }

        //Métodos
        public bool Generar()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();

            string query = "SELECT COALESCE(MAX(numero), 0) numero FROM tickets WHERE fecha_solicitud = CURDATE() AND tipo = '" + ticketEnUso.Tipo.ToString() + "'";

            try
            {
                MySqlDataReader dtrUltimoTicket = dbMySQL.Seleccionar(query);
                if (dtrUltimoTicket.HasRows)
                {
                    if (dtrUltimoTicket.Read())
                    {
                        ticketEnUso.Numero = Convert.ToInt32(dtrUltimoTicket["numero"].ToString()) + 1;
                        dtrUltimoTicket.Close();
                    }
                }
                else
                {
                    ticketEnUso.Numero = 1;
                }

                query = "INSERT INTO tickets(tipo, numero, fecha_solicitud, hora_solicitud, estado, creado, usuario_creador_id)";
                query += " VALUES ('" + ticketEnUso.Tipo + "', " + ticketEnUso.Numero.ToString() + ", CURDATE(), CURTIME(), '0', NOW(), 1)";
                bool resultado = dbMySQL.Insertar(query);

                if (resultado)
                {
                    query = "SELECT tipo, numero, fecha_solicitud, hora_solicitud FROM tickets ORDER BY creado DESC LIMIT 0, 1";
                    dtrUltimoTicket = dbMySQL.Seleccionar(query);
                    if (dtrUltimoTicket.Read())
                    {
                        ticketEnUso.Tipo = dtrUltimoTicket["tipo"].ToString();
                        ticketEnUso.Numero = Convert.ToInt32(dtrUltimoTicket["numero"].ToString());
                        ticketEnUso.FechaSolicitud = dtrUltimoTicket["fecha_solicitud"].ToString().Substring(0, 10);
                        ticketEnUso.HoraSolicitud = dtrUltimoTicket["hora_solicitud"].ToString();
                        dtrUltimoTicket.Close();
                    }
                }
                dbMySQL.CerrarConexion();
                return resultado;
            } catch(Exception e) {
                return false;
            }
        }
    }
}
