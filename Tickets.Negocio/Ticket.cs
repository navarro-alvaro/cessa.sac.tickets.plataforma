﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tickets.Negocio
{
    public class Ticket
    {
        public event MostrarInicioEventHandler MostrarInicio;
        public delegate void MostrarInicioEventHandler();
        public event ErroresEventHandler Errores;
        public delegate void ErroresEventHandler(string mensaje);

        //Instancia de Datos para recuperar información
        Datos.Ticket ticketDato;
        public Ticket()
        {
            ticketDato = new Datos.Ticket();
        }

        //Propiedades
        public string Tipo
        {
            get { return ticketDato.Tipo; }
            set { ticketDato.Tipo = value; }
        }
        public int Numero
        {
            get { return ticketDato.Numero; }
            set { ticketDato.Numero = value; }
        }
        public string FechaSolicitud
        {
            get { return ticketDato.FechaSolicitud; }
            set { ticketDato.FechaSolicitud = value; }
        }
        public string HoraSolicitud
        {
            get { return ticketDato.HoraSolicitud; }
            set { ticketDato.HoraSolicitud = value; }
        }
        public string HoraAtencion
        {
            get { return ticketDato.HoraAtencion; }
            set { ticketDato.HoraAtencion = value; }
        }

        //Métodos
        public void Generar()
        {
            if (ticketDato.Generar())
            {
                MostrarInicio();
            }
            else
            {
                if (Errores != null) Errores("ERROR 1.1: Existe un error al generar el Ticket");
            }
        }
    }
}
